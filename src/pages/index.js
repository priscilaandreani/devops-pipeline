import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';

import styles from './index.module.css';

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className='container'>
        <h1 className='hero__title'>{siteConfig.title}</h1>
        <p className='hero__subtitle'>{siteConfig.tagline}</p>
      </div>
    </header>
  );
}

export default function Home() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={siteConfig.title}
      description='Introdução ao GitLab CI/CD no Ciclo DevOps'>
      <HomepageHeader />
      <main>
        <div className={clsx('hero__subtitle', styles.intro)}>
          Projeto desenvolvido para a disciplina de Cultura e Práticas DevOps,
          associado ao curso de Engenharia de Software da Pontifícia
          Universidade Católica de Minas Gerais (PUC-MG)
        </div>

        <div className={styles.buttons}>
          <Link
            className='button button--secondary button--lg'
            to='docs/pipeline-basics/example'>
            Criando uma pipeline no GitLab - 5min ⏱️
          </Link>
        </div>
      </main>
    </Layout>
  );
}
