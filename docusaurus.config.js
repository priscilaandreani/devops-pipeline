const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Introdução ao GitLab CI/CD no Ciclo DevOps',
  tagline:
    'Aprenda a criar uma pipeline de CI/CD no GitLab para automatizar o processo de build, testes e deploy de uma aplicação web.',
  favicon: 'img/favicon.ico',

  url: 'https://priscilaandreani.gitlab.io/',
  baseUrl: '/devops-pipeline',

  organizationName: 'priscilaandreani',
  projectName: 'devops-pipeline',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  i18n: {
    defaultLocale: 'pt',
    locales: ['pt'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      image: 'img/docusaurus-social-card.jpg',
      navbar: {
        title: 'GitLab CI/CD',
        logo: {
          alt: 'Docussaurus Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'devopsSidebar',
            position: 'left',
            label: 'Projeto',
          },
          {
            href: '/docs/pipeline-basics/example',
            label: 'Exemplo',
            position: 'left',
          },
          {
            href: 'https://gitlab.com/priscilaandreani/devops-pipeline/-/pipelines',
            label: 'GitLab Pipeline',
            position: 'left',
          },
          {
            href: 'https://docusaurus.io/',
            label: 'Docusaurus',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        copyright: `Copyright © ${new Date().getFullYear()} | Priscila Andreani. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
