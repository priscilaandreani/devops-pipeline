---
sidebar_position: 1
---

# Cultura e Práticas DevOps

Pontifícia Universidade Católica de Minas Gerais - PUC Minas

**Engenharia de Software** - Oferta IV – Turma I

---

**Professor:** Ramon Durães

**Alunos:** Felipe da Silva Almeida, Ítalo Emídio Da Silva Vasconcelos, Pablo Rodrigo dos Santos, Priscila Ravanelli Andreani, Silas Ribeiro dos Santos
