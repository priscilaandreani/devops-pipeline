---
sidebar_position: 1
---

# Configurações

Utilize um arquivo YAML para configurar a pipeline e automatizar tarefas relacionadas ao desenvolvimento de Software. Este arquivo deve estar na raíz do projeto e nomeado como `.gitlab-ci.yml`

O arquivo YAML é criado conforme a necessidade do projeto, sendo altamente personalizável. Confira a lista completa das keywords [aqui](https://docs.gitlab.com/ee/ci/yaml/#keywords).

## Diretivas

### Stages

São etapas ou fases que você define para organizar a execução dos jobs em um pipeline. Cada `stage` agrupa `jobs` relacionados que precisam ser executados em uma ordem específica. As `stages` permitem que você controle a sequência de execução e a lógica do seu pipeline de CI/CD.

### Jobs

São as principais unidades de execução no pipeline do GitLab CI/CD. Cada `job` é uma tarefa específica que você deseja executar no pipeline.

### Scripts

Sob a chave `script`, você fornece os comandos que devem ser executados como parte do job. Esses comandos podem ser scripts de shell, comandos de compilação, teste, implantação, etc.

### Rules

Permite definir regras de execução mais avançadas com base em condições complexas. Por exemplo, você pode configurar um job para ser executado apenas se uma combinação específica de variáveis de ambiente estiver presente.

### Dependências

Você pode especificar dependências entre jobs usando a chave dependencies. Isso significa que um job só será executado se suas dependências forem bem-sucedidas.

### Diretivas Condicionais

Você pode usar diretivas condicionais, como `only` e `except`, para controlar quando um job deve ser executado com base em eventos específicos, como branches, tags ou triggers.

### Notificações

Você pode configurar notificações para receber alertas por e-mail ou mensagens de chat quando um job ou pipeline falha.

### Image

A diretiva `image` pode conter uma referência a uma imagem Docker, geralmente em um registro de contêiner, como o Docker Hub, GitLab Container Registry ou qualquer outro registro compatível.

Ela diz ao `runner` qual contêiner usar para executar o trabalho. O `runner`:

- Baixa a imagem do contêiner e a inicia.
- Clona seu projeto GitLab no contêiner em execução.
- Executa os comandos de script, um de cada vez.

## Artifacts

São arquivos ou diretórios gerados durante a execução de jobs no pipeline que são preservados e podem ser usados em jobs subsequentes ou disponibilizados para download após a conclusão do pipeline. Eles são usados para compartilhar saídas importantes de um job com outros jobs, como binários compilados, relatórios de testes, artefatos de implantação ou qualquer outro tipo de dados que você deseja conservar e usar posteriormente.
