---
sidebar_position: 2
---

# Exemplo

O exemplo demonstrado é o utilizado para o build, teste e criação deste projeto.
Você pode encontrar o código base
[aqui](https://gitlab.com/priscilaandreani/devops-pipeline/-/blob/main/.gitlab-ci.yml?ref_type=heads).

O primeiro passo é definir as **etapas** da pipeline e a **ordem de execução**.
Neste caso, iremos criar um job de build, teste e deploy, respectivamente.

```yml
stages:
  - build
  - test
  - deploy
```

Então, podemos criar um valor default para a **imagem** do utilizada.
Por se tratar de uma aplicação web utilizaremos o [Node](https://hub.docker.com/_/node/).

```yml
default:
  image: node
```

Também podemos criar um job "escondido" para definir as regras comuns do projeto,
que será utilizado nas outras etapas:

```yml
.standard-rules:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

Então, partimos para a crição do **build** do projeto.
Para utilizar o job criado no passo anterior extendemos as configurações para o `.standard-rules`.
Evitando duplicação de código nos próximos passos.

```yml
build-job:
  extends:
    - .standard-rules
  stage: build
  script:
    - npm install
    - npm run build
  artifacts:
    paths:
      - 'build/'
```

Após a conclusão do build, seguimos para os **testes** da aplicação.
Aqui serão executados testes de Lint para o Markdown e para o HTML.

Ao adicionar a flag `allow_failure` permitimos que mesmo que o job de Lint do Markdown
falhe a pipeline continue seu ciclo, emitindo apenas um warning.

```yml
lint-markdown:
  stage: test
  extends:
    - .standard-rules
  dependencies: []
  script: # Instalação e execução do pacote Markdownlint CLI
    - npm install markdownlint-cli2 --global
    - markdownlint-cli2 -v
    - markdownlint-cli2 "blog/**/*.md" "docs/**/*.md"
  allow_failure: true
```

Ao adicionar o `build-job` como dependência exigimos que o passo tenha sido concluído
antes da execução do `test-hml`.

```yml
test-html:
  stage: test
  extends:
    - .standard-rules
  dependencies:
    - build-job
  script: ## Instalação e execução do pacote HTML Hint
    - npm install --save-dev htmlhint
    - npx htmlhint --version
    - npx htmlhint build/
```

Por fim, seguimosm para o **deploy** da aplicação.
Nesta etapa:

- Alteramos a imagem default para [Busybox](https://hub.docker.com/_/busybox)
- Adicionamos como depedência obrigatória o `build-job`
- Movemos a pasta `build` para a pasta `public`
- Determinamos que o artefato utilizado será tudo que estiver dentro da pasta `public`
- Adicionamos uma regra que exige que para a execução do job é necessario que o commit tenha sido feito na branch principal

```yml
pages:
  stage: deploy
  image: busybox
  dependencies:
    - build-job
  script:
    - mv build/ public/
  artifacts:
    paths:
      - 'public/'
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

Após a conclusão deste job, aparecerá em o job `pages-deploy`, no qual o GitLab envia
o deploy para o GitLab Sites.
E então será possível acessar a aplicação pela URL do projeto.

![pipeline](img/pipeline.png)
