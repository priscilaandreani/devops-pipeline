# Introdução ao GitLab CI/CD no Ciclo DevOps

Projeto desenvolvido para a disciplina de Cultura e Práticas DevOps, associado ao curso de Engenharia de Software da Pontifícia Universidade Católica de Minas Gerais (PUC-MG)

**Professor:** Ramon Durães

![screen](img/screen.png)
